### Cloud-init for KVM

> **1.** Edit the following files:
>> * meta-data - There is not so strongly neccessary for presence text in this file, but the file itself must be present in image 
>> * user-data - main configuration file where yor write your config
>> * network-config - may include network configuration. Not strongly neccessary. You may exclude them from created image, if you don't need it

> **2.** Next create metadata image after completing configuration. You may exclude network-config file if you not using him, <mark>but the rest two files(meta-data and user-data) must exist anyway, otherwise the image will become inoperable.</mark>

``` genisoimage -o <image_name>.iso -V cidata -r -J user-data meta-data network-config```

> **3.** Mount created image to your VM with installed cloud-init and check messages in log